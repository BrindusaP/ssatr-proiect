package app;

import java.util.concurrent.atomic.AtomicBoolean;

public class Consumer extends Thread {
	
	private User user;
	
	public AtomicBoolean isDone = new AtomicBoolean(false);
	
	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public void run() {
		super.run();
		System.out.println("Consumer: start to consume, remove money for user :" + user.getNume());
		
		while(!isDone.get()){
			try {
				Thread.sleep(2000); // 2s
				
				synchronized(user){
					if(user.getSuma() > 50) {
						user.setSuma(user.getSuma() - 50);	
						System.out.println("Consumer: new sum after consume for user :" + user.getNume() + " is: " + user.getSuma());
					}
					else {
						System.out.println("fonduri insuficiente");
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
