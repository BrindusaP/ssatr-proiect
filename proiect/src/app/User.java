package app;

public class User {

	private int suma;
	private String nume;
	
	public User(int suma, String nume) {
		super();
		this.suma = suma;
		this.nume = nume;
	}
	
	public int getSuma() {
		return suma;
	}
	
	public void setSuma(int suma) {
		this.suma = suma;
	}
	
	public String getNume() {
		return nume;
	}
	
	public void setNume(String nume) {
		this.nume = nume;
	}
}
