package app;

import java.util.concurrent.atomic.AtomicBoolean;

public class Main {

	public static void main(String[] args) throws InterruptedException {
		System.out.println("Test");
		
		User user = new User(0, "userName");
		
		Producer producer = new Producer();
		producer.setUser(user);
		producer.start();
		
		Consumer consumer = new Consumer();
		consumer.setUser(user);
		consumer.start();
		
		Thread.sleep(20000); // aplicatia se opreste la 10 secunde
		producer.isDone = new AtomicBoolean(true);
		consumer.isDone = new AtomicBoolean(true);
		
		// astepta sa termine cele 2 fire de executie
		producer.join();
		consumer.join();
			
		System.out.println("Done!");

	}

}
