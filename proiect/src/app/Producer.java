package app;

import java.util.concurrent.atomic.AtomicBoolean;

public class Producer extends Thread {
	
	private User user;
	
	public AtomicBoolean isDone = new AtomicBoolean(false);

	public void setUser(User user) {
		this.user = user;
	}
	
	@Override
	public void run() {
		super.run();
		System.out.println("Producer: start to produce, add money for user :" + user.getNume());
		
		while(!isDone.get()){
			try {
				Thread.sleep(1000); // 1s
				
				synchronized(user){
					user.setSuma(user.getSuma() + 100);
					System.out.println("Producer: new sum after produce of user :" + user.getNume() + " is: " + user.getSuma());
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
